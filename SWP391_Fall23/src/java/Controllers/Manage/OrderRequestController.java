/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Manage;

import DAL.OrderDAO;
import DAL.OrderDetailsDAO;
import DAL.ProductDAO;
import DAL.UserDAO;
import Model.Constants;
import Model.Order;
import Model.OrderDetails;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 *
 * @author dell
 */
public class OrderRequestController extends HttpServlet {

    final int recordsPerPage = 5;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderRequestController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderRequestController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User acc = (User) request.getSession().getAttribute("account");
        if (acc != null) {
            OrderDetailsDAO odDao = new OrderDetailsDAO();
            OrderDAO oDao = new OrderDAO();
            UserDAO uDao = new UserDAO();

            int page = 1;
            if (request.getParameter("page") != null) {
                page = Integer.parseInt(
                        request.getParameter("page"));
            }

            ArrayList<Order> orders = new ArrayList<>();
            ArrayList<User> sales = new ArrayList<>();
            int noOfRecords;

            if (acc.getRole().getId() == Constants.RoleSaleAdmin) {
                //get all order with status = 1 if the role is admin
                orders = oDao.getOrderByStatus((page - 1) * recordsPerPage,
                        recordsPerPage, Constants.StatusOrderPending);
                noOfRecords = oDao.getNoOfRecords(Constants.StatusOrderPending);
                sales = uDao.getUsersByRoleID(Constants.RoleSale);
            } else {
                //get all order with status = 1
                orders = oDao.getOrderByStatusBySaleId((page - 1) * recordsPerPage,
                        recordsPerPage, Constants.StatusOrderPending, acc.getUserID());
                noOfRecords = noOfRecords = oDao.getNoOfRecordsBySaleId(Constants.StatusOrderPending, acc.getUserID());
            }

            int noOfPages = (int) Math.ceil((double) noOfRecords
                    / recordsPerPage);

            for (Order order : orders) {
                ArrayList<OrderDetails> orderDetails = odDao.getOrderDetailsByOrderID(order.getOrderId());
                order.setOrderDetails(orderDetails);
            }

            request.setAttribute("noOfPages", noOfPages);
            request.setAttribute("currentPage", page);
            request.setAttribute("noOfRecords", noOfRecords);

            request.setAttribute("sales", sales);
            request.setAttribute("orders", orders);
            request.getRequestDispatcher("/views/Manage/OrderRequest.jsp").forward(request, response);
        } else {
            request.getSession().setAttribute("msg", "Bạn cần đăng nhập để vào trang này!");
            response.sendRedirect("home");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderDAO oDao = new OrderDAO();
        OrderDetailsDAO odDao = new OrderDetailsDAO();
        ProductDAO pDao = new ProductDAO();
        
        User acc = (User) request.getSession().getAttribute("account");
        
        String action = request.getParameter("action");
        int OrderId = Integer.parseInt(request.getParameter("id"));
        switch (action) {
            case "accept":
                oDao.setStatusOrder(OrderId, Constants.StatusOrderWait, acc.getUserID());
                doGet(request, response);
                break;
            case "denined":
                oDao.setStatusOrder(OrderId, Constants.StatusOrderDenied, acc.getUserID());

                //get all order detail by orderId
                ArrayList<OrderDetails> odList = odDao.getOrderDetailsByOrderID(OrderId);
                for (OrderDetails orderDetails : odList) {
                    pDao.UpdateProductQuantity(orderDetails.getProduct().getProductId(), orderDetails.getQuantity());
                }

                doGet(request, response);
                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
